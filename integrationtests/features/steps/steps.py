from behave import given, when, then
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


# put it in PO
BASE_XPATH = '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.view.View/android.view.View'
BNT = f'{BASE_XPATH}/android.widget.Button'
NUMBER = f'{BASE_XPATH}/android.view.View[3]'


@when('I click in + button')
def step_impl(context):
    WebDriverWait(context.driver, 10).until(
        EC.element_to_be_clickable((By.XPATH, BNT))
    )

    context.driver.find_element_by_xpath(BNT).click()


@then('increment counter')
def step_impl(context):
    text = context.driver.find_element_by_xpath(NUMBER).text
    assert text == '1'
